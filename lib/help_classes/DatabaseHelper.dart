import 'dart:async';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class DatabaseHelper {
  static const int _dbVersion = 1;

  static Future<void> insert(String table, Map<String, Object> data) async {
    final sqlDb = await _openDb();

    await sqlDb.insert(
      table,
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async{
    final sqlDb = await _openDb();
    return await sqlDb.query(table);
  }
  
  static Future<sql.Database> _openDb() async{
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(
      path.join(dbPath, 'places.db'),
      version: _dbVersion,
      onCreate: (db,version){
        db
        .execute('CREATE TABLE places (id TXT KEY, title TEXT, image TEXT, loc_lat REAL, loc_lng REAL, address TEXT);');
      },
    );
  }
}
